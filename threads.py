# -*- coding: utf-8 -*-

import MySQLdb
import json
import users


# =============================================================================
# Queries
def create_thread(cursor, up_data):
    up_data.update({'isDeleted': up_data.get('isDeleted', 0)})
    try:
        cursor.execute("""INSERT INTO threads (forum, title, user, message,
                                               slug, isClosed, isDeleted, date)
                          VALUES (%(forum)s, %(title)s, %(user)s, %(message)s,
                                  %(slug)s, %(isClosed)s,
                                  %(isDeleted)s, %(date)s);""", up_data)

        cursor.execute("""SELECT * FROM  threads WHERE message=%(message)s
                          AND user=%(user)s;""", up_data)

        down_data = cursor.fetchone()
        down_data.update({'date': str(down_data['date'])})
        # down_data = ''

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def thread_details(cursor, up_data):
    up_data.update({'thread': up_data['thread'][0]})
    related = up_data.get('related', [0, 0])

    if 'thread' in related:
        return json.dumps({'code': 3, 'response': 'Incorrect request.'})
    try:
        cursor.execute("""SELECT * FROM threads
                          WHERE id = %(thread)s;""", up_data)
        down_data = cursor.fetchone()
        down_data.update({'date': str(down_data['date'])})

        if 'user' in related:
            user = users.get_user_details(cursor, down_data['user'])
            down_data.update({'user': user})

        if 'forum' in related:
            cursor.execute("""SELECT * FROM  forums
                              WHERE short_name = %(forum)s;""", down_data)

            forum = cursor.fetchone()
            down_data.update({'forum': forum})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def remove_thread(cursor, up_data):
    try:
        cursor.execute("""UPDATE threads SET isDeleted = true, posts = 0
                          WHERE id = %(thread)s;""", up_data)

        cursor.execute("""UPDATE posts SET isDeleted = true
                          WHERE thread = %(thread)s;""", up_data)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': up_data})


# -----------------------------------------------------------------------------
def restore_thread(cursor, up_data):
    try:
        up_data['count'] = cursor.execute("""UPDATE posts SET isDeleted = false
                                             WHERE thread = %(thread)s;""",
                                          up_data)

        cursor.execute("""UPDATE threads SET isDeleted = false, posts = %(count)s
                          WHERE id = %(thread)s;""", up_data)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': up_data})


# -----------------------------------------------------------------------------
def close_thread(cursor, up_data):
    try:
        cursor.execute("""UPDATE threads SET isClosed = true
                          WHERE id = %(thread)s;""", up_data)

        cursor.execute("""UPDATE posts SET isClosed = true
                          WHERE thread = %(thread)s;""", up_data)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': up_data})


# -----------------------------------------------------------------------------
def open_thread(cursor, up_data):
    try:
        cursor.execute("""UPDATE threads SET isClosed = false
                          WHERE id = %(thread)s;""", up_data)

        cursor.execute("""UPDATE posts SET isClosed = false
                          WHERE thread = %(thread)s;""", up_data)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': up_data})


# -----------------------------------------------------------------------------
def update_thread(cursor, up_data):
    try:
        cursor.execute("""UPDATE threads SET message=%(message)s, slug=%(slug)s
                          WHERE id=%(thread)s;""", up_data)

        cursor.execute("""SELECT * FROM threads WHERE id = %(thread)s;""",
                       up_data)

        down_data = cursor.fetchone()
        down_data.update({'date': str(down_data['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def vote_thread(cursor, up_data):
    try:
        if up_data['vote'] > 0:
            cursor.execute("""UPDATE threads
                              SET likes=likes+1, points=likes-dislikes
                              WHERE id=%(thread)s;""", up_data)
        else:
            cursor.execute("""UPDATE threads
                              SET dislikes=dislikes+1, points=likes-dislikes
                              WHERE id=%(thread)s;""", up_data)

        cursor.execute("""SELECT * FROM  threads WHERE id=%(thread)s;""",
                       up_data)

        down_data = cursor.fetchone()
        down_data.update({'date': str(down_data['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def subscribe_thread(cursor, up_data):
    try:
        cursor.execute("""INSERT INTO subscribes (thread, user)
                          VALUES (%(thread)s, %(user)s);""", up_data)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': up_data})


# -----------------------------------------------------------------------------
def unsubscribe_thread(cursor, up_data):
    try:
        cursor.execute("""DELETE FROM subscribes WHERE thread=%(thread)s
                          AND user=%(user)s""", up_data)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': up_data})


# -----------------------------------------------------------------------------
def list_thread(cursor, up_data):
    up_data.update({'forum': up_data.get('forum', [0, 0])[0]})
    up_data.update({'user': up_data.get('user', [0, 0])[0]})
    up_data.update({'since': up_data.get('since', ['0000-00-00 00:00:00', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', 0])[0]})
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    try:
        if up_data['user']:
            cursor.execute('''SELECT * FROM threads
                              WHERE user=%(user)s AND DATE(date)>=%(since)s
                              ORDER BY date ''' + asc_desc + ''' LIMIT %(limit)s;'''
                           , up_data)

        if up_data['forum']:
            if up_data['order'] == 'desc':
                cursor.execute('''SELECT * FROM threads
                                  WHERE forum=%(forum)s AND DATE(date)>=%(since)s
                                  ORDER BY date ''' + asc_desc + ''' LIMIT %(limit)s;'''
                               , up_data)

        down_data = [i for i in cursor.fetchall()]
        for thread in down_data:
            thread.update({'date': str(thread['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def listPosts_thread(cursor, up_data):
    up_data.update({'thread': up_data['thread'][0]})
    up_data.update({'since': up_data.get('since', ['0000-00-00 00:00:00', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', 0])[0]})
    sort = up_data.get('sort', [0, 0, 0])
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    min_max = 'MAX' if up_data['order'] == 'asc' else 'MIN'
    sign = '<' if up_data['order'] == 'asc' else '>'
    try:
        if 'tree' in sort:
            sql = '''SELECT * FROM posts
                     WHERE thread=%(thread)s AND DATE(date)>=%(since)s
                     ORDER BY CAST(path as unsigned) ''' + asc_desc + ''' ,
                     inet_aton(path) LIMIT %(limit)s;'''
            cursor.execute(sql, up_data)

        elif 'parent_tree' in sort:
            sql = '''SELECT ''' + min_max + '''(path) from (
                        SELECT path FROM posts
                        WHERE thread=%(thread)s AND DATE(date)>=%(since)s
                        AND id=path ORDER BY path ''' + asc_desc + '''
                        LIMIT %(limit)s
                     ) as t;'''
            cursor.execute(sql, up_data)
            up_data = {'last_post': cursor.fetchone()}

            sql = '''SELECT * FROM posts
                     WHERE thread=%(thread)s AND DATE(date)>=%(since)s
                     AND CAST(path as unsigned)''' + sign + '''=%('last_post')s
                     ORDER BY CAST(path as unsigned) DESC, INET_ATON(path)
                     LIMIT %(limit)s;'''
            cursor.execute(sql, up_data)
        else:
            sql = '''SELECT * FROM posts
                     WHERE thread=%(thread)s AND DATE(date)>=%(since)s
                     ORDER BY date ''' + asc_desc + ''' LIMIT %(limit)s;'''
            cursor.execute(sql, up_data)

        down_data = [i for i in cursor.fetchall()]
        for post in down_data:
            post.update({'date': str(post['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


