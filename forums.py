# -*- coding: utf-8 -*-

import MySQLdb, json
import users

# =============================================================================
# Queries
def create_forum(cursor, up_data):
    try:
        cursor.execute("""INSERT INTO forums (name, short_name, user)
                          VALUES (%(name)s, %(short_name)s, %(user)s)""", up_data)

        cursor.execute("""SELECT * FROM forums
                          WHERE short_name=%(short_name)s""", up_data)
        down_data = cursor.fetchone()
        # down_data = ''

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def forum_details(cursor, up_data):
    up_data.update({'forum': up_data['forum'][0]})
    related = up_data.get('related', [0])
    try:
        cursor.execute("""SELECT * FROM  forums
                          WHERE short_name = %(forum)s;""", up_data)

        down_data = cursor.fetchone()
        if 'user' in related:
            user = users.get_user_details(cursor, down_data['user'])
            down_data.update({'user': user})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def listPosts_forum(cursor, up_data):
    up_data.update({'forum': up_data['forum'][0]})
    up_data.update({'since': up_data.get('since', ['0000-00-00 00:00:00', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', 0])[0]})
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    try:
        cursor.execute('''SELECT * FROM posts
                          WHERE forum=%(forum)s AND DATE(date)>=%(since)s
                          ORDER BY date ''' + asc_desc + ''' LIMIT %(limit)s;'''
                       , up_data)

        down_data = [i for i in cursor.fetchall()]
        for post in down_data:
            if 'thread' in up_data.get('related', [0]):
                cursor.execute("""SELECT * FROM  threads
                                  WHERE id = %(thread)s;""", post)
                thread = cursor.fetchone()
                thread.update({'date': str(thread['date'])})
                post.update({'thread': thread})

            if 'forum' in up_data.get('related', [0]):
                cursor.execute("""SELECT * FROM  forums
                                  WHERE short_name = %(forum)s;""", post)
                forum = cursor.fetchone()
                post.update({'forum': forum})

            if 'user' in up_data.get('related', [0]):
                user = users.get_user_details(cursor, post['user'])
                post.update({'user': user})

            post.update({'date': str(post['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def listThreads_forum(cursor, up_data):
    up_data.update({'forum': up_data['forum'][0]})
    up_data.update({'since': up_data.get('since', ['0000-00-00 00:00:00', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', 0])[0]})
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    try:
        cursor.execute('''SELECT * FROM threads
                          WHERE forum=%(forum)s AND DATE(date)>=%(since)s
                          ORDER BY date ''' + asc_desc + ''' LIMIT %(limit)s;'''
                       , up_data)

        down_data = [i for i in cursor.fetchall()]
        for thread in down_data:
            if 'user' in up_data.get('related', [0]):
                user = users.get_user_details(cursor, thread['user'])
                thread.update({'user': user})

            if 'forum' in up_data.get('related', [0]):
                cursor.execute("""SELECT * FROM  forums
                                  WHERE short_name = %(forum)s;""", thread)
                forum = cursor.fetchone()
                thread.update({'forum': forum})

            thread.update({'date': str(thread['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def listUsers_forum(cursor, up_data):
    up_data.update({'forum': up_data['forum'][0]})
    up_data.update({'since_id': up_data.get('since_id', ['0', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', 0])[0]})
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    try:
        cursor.execute('''SELECT u.* FROM users AS u
                          INNER JOIN posts AS p ON u.email=p.user
                          WHERE forum=%(forum)s AND u.id>=%(since_id)s
                          GROUP BY email, forum
                          ORDER BY name ''' + asc_desc + ''' LIMIT %(limit)s;'''
                       , up_data)

        down_data = [i for i in cursor.fetchall()]
        for user in down_data:
            users.get_follow_subscribe_details(cursor, user['email'], user)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})