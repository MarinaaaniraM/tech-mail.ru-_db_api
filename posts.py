# -*- coding: utf-8 -*-

import MySQLdb
import json
import users


# =============================================================================
# Queries
def create_post(cursor, up_data):
    up_data.update({'parent': up_data.get('parent', None)})
    up_data.update({'isApproved': up_data.get('isApproved', False)})
    up_data.update({'isHighlighted': up_data.get('isHighlighted', False)})
    up_data.update({'isEdited': up_data.get('isEdited', False)})
    up_data.update({'isSpam': up_data.get('isSpam', False)})
    up_data.update({'isDeleted': up_data.get('isDeleted', False)})
    try:
        cursor.execute('''INSERT INTO posts (date, thread, message, user, forum,
                                             parent, isApproved, isHighlighted,
                                             isEdited, isSpam, isDeleted)
                          VALUES (%(date)s, %(thread)s, %(message)s, %(user)s,
                                  %(forum)s, %(parent)s, %(isApproved)s,
                                  %(isHighlighted)s, %(isEdited)s, %(isSpam)s,
                                  %(isDeleted)s);''', up_data)
        last_id = cursor.lastrowid

        if up_data['parent'] is None:
            cursor.execute('''UPDATE posts SET path=id
                              WHERE id=%s''', str(last_id))
        else:
            cursor.execute('''SELECT path, countChilds FROM posts
                              WHERE id=%s''', up_data['parent'])
            parent = cursor.fetchone()
            data = {'id': last_id,
                    'curr_path': parent['path'] + '.' + str(parent['countChilds'] + 1)}

            cursor.execute('''UPDATE posts SET path=%(curr_path)s
                              WHERE id=%(id)s''', data)
            cursor.execute('''UPDATE posts SET countChilds=countChilds+1
                              WHERE id=%(parent)s''', up_data)

        cursor.execute('''UPDATE threads SET posts=posts+1
                          WHERE id=%(thread)s;''', up_data)

        cursor.execute('''SELECT * FROM  posts WHERE id=%s;''', last_id)
        down_data = cursor.fetchone()
        down_data.update({'date': str(down_data['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def post_details(cursor, up_data):
    up_data.update({'post': up_data['post'][0]})
    related = up_data.get('related', [0, 0, 0])

    if int(up_data['post']) < 1:
        return json.dumps({'code': 1,
                           'response': 'Requested object was not found.'})
    try:
        cursor.execute('''SELECT * FROM  posts WHERE id = %(post)s;''', up_data)
        down_data = cursor.fetchone()
        down_data.update({'date': str(down_data['date'])})

        if 'user' in related:
            user = users.get_user_details(cursor, down_data['user'])
            down_data.update({'user': user})

        if 'forums' in related:
            cursor.execute('''SELECT * FROM  forums
                              WHERE short_name = %(forum)s;''', down_data)
            forum = cursor.fetchone()
            down_data.update({'forum': forum})

        if 'thread' in related:
            cursor.execute('''SELECT * FROM  threads
                              WHERE id = %(thread)s;''', down_data)
            thread = cursor.fetchone()
            thread.update({'date': str(thread['date'])})
            down_data.update({'thread': thread})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def remove_post(cursor, up_data):
    try:
        cursor.execute('''UPDATE posts SET isDeleted = true
                          WHERE id = %(post)s;''', up_data)

        cursor.execute('''SELECT thread FROM posts
                          WHERE id = %(post)s;''', up_data)
        thread = cursor.fetchone()

        cursor.execute('''UPDATE threads SET posts = posts - 1
                          WHERE id = %(thread)s;''', thread)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': {'post': up_data['post']}})


# -----------------------------------------------------------------------------
def restore_post(cursor, up_data):
    try:
        cursor.execute('''UPDATE posts SET isDeleted = false
                          WHERE id = %(post)s;''', up_data)

        cursor.execute('''SELECT thread FROM posts
                          WHERE id = %(post)s;''', up_data)
        thread = cursor.fetchone()

        cursor.execute('''UPDATE threads SET posts = posts + 1
                          WHERE id = %(thread)s;''', thread)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': up_data})


# -----------------------------------------------------------------------------
def update_post(cursor, up_data):
    try:
        cursor.execute('''UPDATE posts SET message = %(message)s
                          WHERE id = %(post)s;''', up_data)

        cursor.execute('''SELECT * FROM posts WHERE id = %(post)s;''', up_data)
        down_data = cursor.fetchone()
        down_data.update({'date': str(down_data['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def vote_post(cursor, up_data):
    try:
        if up_data['vote'] > 0:
            cursor.execute('''UPDATE posts
                              SET likes = likes+1, points=likes-dislikes
                              WHERE id = %(post)s;''', up_data)
        else:
            cursor.execute('''UPDATE posts
                              SET dislikes=dislikes+1, points=likes-dislikes
                              WHERE id = %(post)s;''', up_data)

        cursor.execute('''SELECT * FROM  posts WHERE id = %(post)s''', up_data)
        down_data = cursor.fetchone()
        down_data.update({'date': str(down_data['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def list_post(cursor, up_data):
    up_data.update({'forum': up_data.get('forum', [0, 0])[0]})
    up_data.update({'thread': up_data.get('thread', [0, 0])[0]})
    up_data.update({'since': up_data.get('since', ['0000-00-00 00:00:00', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', '0'])[0]})
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    try:
        if up_data['thread']:
            cursor.execute('''SELECT * FROM posts
                              WHERE thread=%(thread)s AND DATE(date)>=%(since)s
                              ORDER BY date ''' + asc_desc + ''' LIMIT %(limit)s;'''
                           , up_data)
        if up_data['forum']:
            cursor.execute('''SELECT * FROM posts
                              WHERE forum=%(forum)s AND DATE(date)>=%(since)s
                              ORDER BY date ''' + asc_desc + ''' LIMIT %(limit)s;'''
                           , up_data)

        down_data = [i for i in cursor.fetchall()]
        for post in down_data:
            post.update({'date': str(post['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})

