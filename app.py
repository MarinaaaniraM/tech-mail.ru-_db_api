import flask
import sql
import users
import forums
import posts
import threads

app = flask.Flask(__name__)

@app.route('/')
def hello():
    return 'Hello World!'


# =============================================================================
# CLEAR
@app.route('/db/api/clear/', methods=['POST'])
def clear():
    database = sql.open_db()
    sql.clear(database['cursor'])
    sql.close_db(database['db'], database['cursor'])
    return flask.jsonify(code=0, response='OK')


# =============================================================================
# USER
@app.route('/db/api/user/create/', methods=['POST'])
def user_create():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = users.create_user(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/user/details/', methods=['GET'])
def user_details():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = users.user_details(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/user/updateProfile/', methods=['POST'])
def user_update():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = users.update_profile(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/user/follow/', methods=['POST'])
def user_follow():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = users.follow_user(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/user/unfollow/', methods=['POST'])
def user_unfollow():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = users.unfollow_user(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/user/listPosts/', methods=['GET'])
def user_listPosts():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = users.listPosts_user(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/user/listFollowers/', methods=['GET'])
def user_listFollowers():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = users.listFollowers_user(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/user/listFollowing/', methods=['GET'])
def user_listFollowing():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = users.listFollowing_user(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data


# =============================================================================
# FORUM
@app.route('/db/api/forum/create/', methods=['POST'])
def forum_create():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = forums.create_forum(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/forum/details/', methods=['GET'])
def forum_details():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = forums.forum_details(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/forum/listPosts/', methods=['GET'])
def forum_listPosts():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = forums.listPosts_forum(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/forum/listThreads/', methods=['GET'])
def forum_listThreads():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = forums.listThreads_forum(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/forum/listUsers/', methods=['GET'])
def forum_listUsers():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = forums.listUsers_forum(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data


# =============================================================================
# POST
@app.route('/db/api/post/create/', methods=['POST'])
def post_create():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = posts.create_post(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/post/details/', methods=['GET'])
def post_details():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = posts.post_details(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/post/remove/', methods=['POST'])
def post_remove():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = posts.remove_post(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/post/restore/', methods=['POST'])
def post_restore():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = posts.restore_post(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/post/update/', methods=['POST'])
def post_update():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = posts.update_post(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/post/vote/', methods=['POST'])
def post_vote():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = posts.vote_post(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/post/list/', methods=['GET'])
def post_list():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = posts.list_post(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data


# =============================================================================
# THREAD
@app.route('/db/api/thread/create/', methods=['POST'])
def thread_create():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.create_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/details/', methods=['GET'])
def thread_details():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = threads.thread_details(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/remove/', methods=['POST'])
def thread_remove():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.remove_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/restore/', methods=['POST'])
def thread_restore():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.restore_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/close/', methods=['POST'])
def thread_close():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.close_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/open/', methods=['POST'])
def thread_open():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.open_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/update/', methods=['POST'])
def thread_update():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.update_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/vote/', methods=['POST'])
def thread_vote():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.vote_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/subscribe/', methods=['POST'])
def thread_subscribe():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.subscribe_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/unsubscribe/', methods=['POST'])
def thread_unsubscribe():
        up_data = flask.request.json
        database = sql.open_db()

        down_data = threads.unsubscribe_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/list/', methods=['GET'])
def thread_list():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = threads.list_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

@app.route('/db/api/thread/listPosts/', methods=['GET'])
def thread_listPosts():
        up_data = dict(flask.request.args)
        database = sql.open_db()

        down_data = threads.listPosts_thread(database['cursor'], up_data)

        sql.close_db(database['db'], database['cursor'])
        return down_data

app.run(debug=True)
# if __name__ == "__main__":
#     app.run()
