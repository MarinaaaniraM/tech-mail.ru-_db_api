# -*- coding: utf-8 -*-

import MySQLdb


# =============================================================================
def open_db():
    db = MySQLdb.connect(host="localhost",
                         user="marina",
                         passwd="890",
                         charset='utf8',)
    cursor = db.cursor(MySQLdb.cursors.DictCursor)
    sql = """USE tp_db"""
    cursor.execute(sql)
    return {'cursor': cursor, 'db': db}



# =============================================================================
def close_db(db, cursor):
    db.commit()
    cursor.close()
    db.close()


# =============================================================================
def clear(cursor):
    cursor.execute("""DROP TABLE IF EXISTS users;""")
    cursor.execute("""DROP TABLE IF EXISTS forums;""")
    cursor.execute("""DROP TABLE IF EXISTS posts;""")
    cursor.execute("""DROP TABLE IF EXISTS threads;""")
    cursor.execute("""DROP TABLE IF EXISTS subscribes;""")
    cursor.execute("""DROP TABLE IF EXISTS follows;""")

    cursor.execute("""CREATE TABLE users (
                      id          INT AUTO_INCREMENT PRIMARY KEY   NOT NULL,
                      username    CHAR(25),
                      name        CHAR(25),
                      email       CHAR(25)           UNIQUE        NOT NULL,
                      about       TEXT,
                      isAnonymous BOOLEAN            DEFAULT FALSE NOT NULL,

                      KEY (email)
                  );""")

    cursor.execute("""CREATE TABLE forums (
                      id          INT AUTO_INCREMENT PRIMARY KEY  NOT NULL,
                      name        CHAR(35)            UNIQUE      NOT NULL ,
                      short_name  CHAR(35)            UNIQUE      NOT NULL ,
                      user        CHAR(25)                        NOT NULL,

                      KEY (short_name)
                  );""")

    cursor.execute("""CREATE TABLE threads (
                        id        INT AUTO_INCREMENT PRIMARY KEY    NOT NULL,
                        title     CHAR(50)                          NOT NULL,
                        forum     CHAR(35)                          NOT NULL,
                        user      CHAR(25)                          NOT NULL,
                        slug      CHAR(50)                          NOT NULL,
                        date      DATETIME                          NOT NULL,
                        message   TEXT                              NOT NULL,
                        likes     INT                 DEFAULT 0     NOT NULL,
                        dislikes  INT                 DEFAULT 0     NOT NULL,
                        points    INT                 DEFAULT 0     NOT NULL,
                        posts     INT                 DEFAULT 0     NOT NULL,
                        isDeleted BOOLEAN             DEFAULT FALSE NOT NULL,
                        isClosed  BOOLEAN             DEFAULT FALSE NOT NULL,

                        KEY forum_date (forum, date),
                        KEY user_date (user, date)
                      );""")

    cursor.execute("""CREATE TABLE posts (
                      id            INT AUTO_INCREMENT PRIMARY KEY    NOT NULL,
                      date          DATETIME                          NOT NULL,
                      thread        INT                               NOT NULL,
                      message       TEXT                              NOT NULL,
                      user          CHAR(25)                          NOT NULL,
                      forum         CHAR(35)                          NOT NULL,
                      likes         INT                 DEFAULT 0     NOT NULL,
                      dislikes      INT                 DEFAULT 0     NOT NULL,
                      points        INT                 DEFAULT 0     NOT NULL,
                      parent        INT                 DEFAULT NULL,
                      isApproved    BOOLEAN             DEFAULT FALSE NOT NULL,
                      isHighlighted BOOLEAN             DEFAULT FALSE NOT NULL,
                      isEdited      BOOLEAN             DEFAULT FALSE NOT NULL,
                      isSpam        BOOLEAN             DEFAULT FALSE NOT NULL,
                      isDeleted     BOOLEAN             DEFAULT FALSE NOT NULL,
                      countChilds   INT                 DEFAULT 0,
                      path          CHAR(50)                          NOT NULL,

                      KEY user_date (user, date),
                      KEY forum_date (forum, date),
                      KEY thread_date (thread, date)
                  );""")

    cursor.execute("""CREATE TABLE follows (
                      follower CHAR(25) NOT NULL,
                      followee CHAR(25) NOT NULL,

                      PRIMARY KEY (follower, followee),
                      KEY follow (followee, follower)
                  );""")

    cursor.execute("""CREATE TABLE subscribes (
                      user    CHAR(25)  NOT NULL,
                      thread  INT       NOT NULL,

                      PRIMARY KEY (user, thread)
                  )""")
