# -*- coding: utf-8 -*-

import MySQLdb
import json


# =============================================================================
# Helpers
def get_follow_subscribe_details(cursor, email, need_update_user):
    cursor.execute("""SELECT DISTINCT followee FROM follows
                      WHERE follower = %s;""", email)
    following = [i['followee'] for i in cursor.fetchall()]

    cursor.execute("""SELECT DISTINCT follower FROM follows
                      WHERE followee = %s;""", email)
    followers = [i['follower'] for i in cursor.fetchall()]

    cursor.execute("""SELECT DISTINCT thread FROM subscribes
                      WHERE user = %s""", email)
    subscribes = [i['thread'] for i in cursor.fetchall()]

    need_update_user.update({'followers': followers,
                             'following': following,
                             'subscriptions': subscribes})
    return need_update_user


# -----------------------------------------------------------------------------
def get_user_details(cursor, email):
    cursor.execute("""SELECT * FROM  users WHERE email=%s;""", email)
    user = cursor.fetchone()

    user = get_follow_subscribe_details(cursor, email, user)
    return user



# =============================================================================
# Queries
def create_user(cursor, up_data):
    up_data.update({'isAnonymous': up_data.get('isAnonymous', 'False')})
    try:
        cursor.execute('''INSERT INTO users (username, name, email,
                                             about, isAnonymous)
                          VALUES (%(username)s, %(name)s, %(email)s,
                                  %(about)s, %(isAnonymous)s);''', up_data)

        down_data = get_user_details(cursor, up_data['email'])
        # down_data = ''

    except MySQLdb.Error as err:
        if err.args[0] == 1062:
            return json.dumps({'code': 5,
                               'response': 'Requested object was not found'})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def user_details(cursor, up_data):
    up_data.update({'user': up_data['user'][0]})
    try:
        down_data = get_user_details(cursor, up_data['user'])

    except MySQLdb.Error as err:
        return json.dumps({'code': err.args[0], 'response': err.args[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def update_profile(cursor, up_data):
    try:
        cursor.execute('''UPDATE users SET name = %(name)s, about = %(about)s
                          WHERE email = %(user)s;''', up_data)

        down_data = get_user_details(cursor, up_data['user'])

    except MySQLdb.Error as err:
        return json.dumps({'code': err.args[0], 'response': err.args[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def follow_user(cursor, up_data):
    try:
        cursor.execute('''INSERT INTO follows (follower, followee)
                          VALUES (%(follower)s, %(followee)s)''', up_data)

        down_data = get_user_details(cursor, up_data['follower'])

    except MySQLdb.Error as err:
        return json.dumps({'code': err.args[0], 'response': err.args[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def unfollow_user(cursor, up_data):
    try:
        cursor.execute('''DELETE FROM follows WHERE follower = %(follower)s
                          AND followee = %(followee)s''', up_data)

        down_data = get_user_details(cursor, up_data['follower'])

    except MySQLdb.Error as err:
        return json.dumps({'code': err.args[0], 'response': err.args[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def listPosts_user(cursor, up_data):
    up_data.update({'user': up_data['user'][0]})
    up_data.update({'since': up_data.get('since', ['0000-00-00 00:00:00', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', 0])[0]})
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    try:
        cursor.execute('''SELECT * FROM posts WHERE user=%(user)s
                          AND DATE(date)>=%(since)s
                          ORDER BY date ''' + asc_desc + ''' LIMIT %(limit)s;'''
                       , up_data)

        down_data = [i for i in cursor.fetchall()]

        for post in down_data:
            post.update({'date': str(post['date'])})

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def listFollowers_user(cursor, up_data):
    up_data.update({'user': up_data['user'][0]})
    up_data.update({'since_id': up_data.get('since_id', ['0', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', 0])[0]})
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    try:
        cursor.execute('''SELECT DISTINCT u.* FROM  users AS u
                          INNER JOIN follows AS f ON u.email=f.follower
                          AND f.followee=%(user)s WHERE u.id>=%(since_id)s
                          ORDER BY name ''' + asc_desc + ''' LIMIT %(limit)s;'''
                       , up_data)

        down_data = [i for i in cursor.fetchall()]
        for user in down_data:
            get_follow_subscribe_details(cursor, user['email'], user)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})


# -----------------------------------------------------------------------------
def listFollowing_user(cursor, up_data):
    up_data.update({'user': up_data['user'][0]})
    up_data.update({'since_id': up_data.get('since_id', ['0', 0])[0]})
    up_data.update({'limit': up_data.get('limit', [18446744073709551615, 0])[0]})
    up_data.update({'limit': long(up_data['limit'])})
    up_data.update({'order': up_data.get('order', ['DESC', 0])[0]})
    asc_desc = 'ASC' if up_data['order'] == 'asc' else 'DESC'
    try:
        cursor.execute('''SELECT DISTINCT u.* FROM  users AS u
                          INNER JOIN follows AS f ON u.email=f.followee
                          AND f.follower=%(user)s WHERE u.id>=%(since_id)s
                          ORDER BY name ''' + asc_desc + ''' LIMIT %(limit)s;'''
                       , up_data)

        down_data = [i for i in cursor.fetchall()]
        for user in down_data:
            get_follow_subscribe_details(cursor, user['email'], user)

    except MySQLdb.Error as err:
        return json.dumps({'code': err[0], 'response': err[1]})

    return json.dumps({'code': 0, 'response': down_data})
